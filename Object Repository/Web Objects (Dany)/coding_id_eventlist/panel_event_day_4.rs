<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>panel_event_day_4</name>
   <tag></tag>
   <elementGuidId>3e8aeff4-4b4f-4698-b42f-7d38bcd07145</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//li[@id='blockListEvent']/a/div)[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>c7e4939a-7ff3-44a2-b9b3-f397471c4f63</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>cardOuter</value>
      <webElementGuid>1ab99fe3-a6f8-48c8-9ebf-94315391af59</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                        
                                            
                                                
                                                                                                    
                                                    
                                                    
                                                                                            

                                            
                                                
                                                    Day 4: Workshop
                                                    
                                                
                                                
                                                    With Ziyad Syauqi Fawwazi
                                                    
                                                
                                            

                                        
                                        
                                                                                            OPEN until
                                                        25 Nov 2023 11:30 WIB
                                                
                                                                                        Mini Class
                                            
                                            
                                                Day 4: Workshop
                                                
                                            
                                            
                                                25 Nov 2023 |
                                                19:30 WIB Via
                                                Zoom
                                                
                                            
                                                                                                                                                Rp.
                                                            500.000

                                                                                                                    Rp
                                                                85.000
                                                                                                            
                                                                                                                                    
                                    </value>
      <webElementGuid>eadc40f0-b327-425e-820c-1aaf53e346c9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;loopevent&quot;)/li[@id=&quot;blockListEvent&quot;]/a[1]/div[@class=&quot;cardOuter&quot;]</value>
      <webElementGuid>86f97751-dcb1-41e1-93a0-94cbd716fcc9</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>(//li[@id='blockListEvent']/a/div)[2]</value>
      <webElementGuid>0cd7d9b0-a943-4b8b-a69d-180e8e756eb0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[2]/a/div</value>
      <webElementGuid>87319a22-bc9d-4379-b017-cfeeda92dc7e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                                        
                                            
                                                
                                                                                                    
                                                    
                                                    
                                                                                            

                                            
                                                
                                                    Day 4: Workshop
                                                    
                                                
                                                
                                                    With Ziyad Syauqi Fawwazi
                                                    
                                                
                                            

                                        
                                        
                                                                                            OPEN until
                                                        25 Nov 2023 11:30 WIB
                                                
                                                                                        Mini Class
                                            
                                            
                                                Day 4: Workshop
                                                
                                            
                                            
                                                25 Nov 2023 |
                                                19:30 WIB Via
                                                Zoom
                                                
                                            
                                                                                                                                                Rp.
                                                            500.000

                                                                                                                    Rp
                                                                85.000
                                                                                                            
                                                                                                                                    
                                    ' or . = '
                                        
                                            
                                                
                                                                                                    
                                                    
                                                    
                                                                                            

                                            
                                                
                                                    Day 4: Workshop
                                                    
                                                
                                                
                                                    With Ziyad Syauqi Fawwazi
                                                    
                                                
                                            

                                        
                                        
                                                                                            OPEN until
                                                        25 Nov 2023 11:30 WIB
                                                
                                                                                        Mini Class
                                            
                                            
                                                Day 4: Workshop
                                                
                                            
                                            
                                                25 Nov 2023 |
                                                19:30 WIB Via
                                                Zoom
                                                
                                            
                                                                                                                                                Rp.
                                                            500.000

                                                                                                                    Rp
                                                                85.000
                                                                                                            
                                                                                                                                    
                                    ')]</value>
      <webElementGuid>28465e99-1e26-4f79-8004-400eaeb3e3ed</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
