<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_already_bought</name>
   <tag></tag>
   <elementGuidId>a3b65ae1-bb31-4a5e-a23e-b461f65ab96a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button.btn.btn-primary.btn-lg</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//li[@id='list-button']/button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>01fc1e9b-fc44-43cb-bb93-3f47f41da773</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-primary btn-lg</value>
      <webElementGuid>c282e6b6-2a6f-482a-9285-64381f626596</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                You are joined</value>
      <webElementGuid>b05c30f7-7acf-436b-beb9-72a64b7f0176</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;list-button&quot;)/button[@class=&quot;btn btn-primary btn-lg&quot;]</value>
      <webElementGuid>6aaa4c85-86b5-4e8b-b524-a49c091cdabb</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//li[@id='list-button']/button</value>
      <webElementGuid>4d2af5a1-6176-4bd6-9ab5-477fa4d43b00</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Detik'])[1]/following::button[1]</value>
      <webElementGuid>e7aa3fc1-2ce1-4160-9cf4-92105d668443</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Menit'])[1]/following::button[1]</value>
      <webElementGuid>e9ba1a04-a0ec-4a04-a043-75ced8d8b7c5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Event lain yang banyak diminati'])[1]/preceding::button[1]</value>
      <webElementGuid>ed6e48e9-1169-4841-a01c-0a901051b350</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Day 4: Workshop'])[1]/preceding::button[1]</value>
      <webElementGuid>13c23975-db4d-43bb-a823-5c0aa2cf1325</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='You are joined']/parent::*</value>
      <webElementGuid>21a7f3eb-fbf1-4b84-9050-8e92f98e05e8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[6]/button</value>
      <webElementGuid>9ae6bd7c-28cd-44e9-a1d2-0b1302082683</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[(text() = '
                                                You are joined' or . = '
                                                You are joined')]</value>
      <webElementGuid>60f7a48d-55e6-48ff-9271-66c0b5bcaae8</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
