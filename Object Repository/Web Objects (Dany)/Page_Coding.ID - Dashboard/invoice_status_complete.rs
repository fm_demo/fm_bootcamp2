<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>invoice_status_complete</name>
   <tag></tag>
   <elementGuidId>b5f6be63-ae1f-4b40-b0a7-79c034d0974b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>span.badge.badge-success</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='badge-status']/h5/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>70647a3f-0d64-4da4-bc5b-c24a3fe83847</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>badge badge-success</value>
      <webElementGuid>7624d37b-4266-4546-8845-18e54302623a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Completed</value>
      <webElementGuid>2d01d7a0-e953-4484-a673-d0e42a42b0c2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;badge-status&quot;)/h5[1]/span[@class=&quot;badge badge-success&quot;]</value>
      <webElementGuid>49601537-30fe-4578-9806-4752a5c714c0</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='badge-status']/h5/span</value>
      <webElementGuid>639c9d81-dca5-400f-9b97-fcc2c6d4b8c3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='#EVN11072300001'])[1]/following::span[1]</value>
      <webElementGuid>df19d457-e5e9-40b5-b747-9e8602b4ea48</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Back'])[1]/following::span[1]</value>
      <webElementGuid>e30454dc-9b73-432e-b41e-48aef8325ab2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Bill To :'])[1]/preceding::span[1]</value>
      <webElementGuid>eef05fb7-2f1d-4334-bb63-5bd006a53735</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Test user'])[1]/preceding::span[1]</value>
      <webElementGuid>a3becf98-0335-4b76-9019-a87a29b5d73c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Completed']/parent::*</value>
      <webElementGuid>1e45cbdf-7ea2-4564-9585-192f1504068d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h5/span</value>
      <webElementGuid>ea0d83c7-2bc1-42bd-9213-8bee3c39ad49</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Completed' or . = 'Completed')]</value>
      <webElementGuid>419a7818-8598-499a-9adc-3eceff6c8710</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
