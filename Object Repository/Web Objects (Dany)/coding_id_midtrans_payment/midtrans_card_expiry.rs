<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>midtrans_card_expiry</name>
   <tag></tag>
   <elementGuidId>c4b9d583-cc00-415f-b39c-d8b698b55ec3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#card-expiry</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='card-expiry']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>42bad056-ba98-467d-ba33-af813463af1c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>card-expiry</value>
      <webElementGuid>8f0fa9e0-c84a-46dd-8f73-e9b8836751df</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>tel</value>
      <webElementGuid>f8ebc14a-8d4e-43f8-8487-16ddc29d6d83</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>cc-exp</value>
      <webElementGuid>3f37d645-5c5b-4e09-94ee-fac5a5845f6e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocorrect</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>224b6a5b-4760-40cc-a23d-4d1820d4a49b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>spellcheck</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>4b102e30-b307-4c84-ba2b-64ff61ce86aa</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>valid-input-value</value>
      <webElementGuid>cbe118be-7a60-434a-a208-150caa54e248</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>0</value>
      <webElementGuid>b187b22c-4cbf-424b-bd91-97068427c1c0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;card-expiry&quot;)</value>
      <webElementGuid>096fccee-d1ec-4b4e-9fb3-754ea497b20c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Web Objects (Dany)/iframe_concat(id(, , snap-midtrans, , ))_po_08c6ad</value>
      <webElementGuid>88e613bd-b042-4c42-907f-52f5a94d2ecb</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='card-expiry']</value>
      <webElementGuid>b357110c-bcad-44ba-acd5-f03db0075742</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='application']/div/div/div/div[3]/div/input</value>
      <webElementGuid>9667d77a-7648-4571-bb2d-52db47e315f1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/input</value>
      <webElementGuid>718fd143-c46f-433f-bf47-42bee2d6379a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'card-expiry' and @type = 'tel']</value>
      <webElementGuid>21faa5f8-2962-4cb9-b9cb-9162e2e32323</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
